package coche;

public class Todoterreno extends Automovil {

	
	public double getConsumo() { return consumo * 1.2; }

	@Override
	public String toString() {
		return "Todoterreno [consumo=" + getConsumo() + ", marca=" + marca + ", toString()=" + super.toString() + "]";
	}
	
	
	
}

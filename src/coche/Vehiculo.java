package coche;

public class Vehiculo {
	
	
		protected String marca;
		
		// Getters / Setters
		public void setMarca(String m) { marca = m; }
		public String getMarca() { return marca; }
		
		
		@Override
		public String toString() {
			return "Vehiculo [marca=" + marca + ", toString()=" + super.toString() + "]";
		}
		
		
		
		
}

package coche;

public class Bicicleta extends Vehiculo {

	private String talla;

	public String getTalla() {
		return talla;
	}

	public void setTalla(String talla) {
		this.talla = talla;
	}

	@Override
	public String toString() {
		return "bicicleta [talla=" + talla + ", getMarca()=" + getMarca() + "]";
	}
	
	
	
	
}

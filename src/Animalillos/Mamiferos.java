package Animalillos;

public class Mamiferos extends Animal {
	
		String colorPelo;

		public String getColorPelo() {
			if(this.colorPelo == null)
			{
				return "Es un bicho peloncitoooo";
			}
				
			return colorPelo;
		}

		public void setColorPelo(String colorPelo) {
			this.colorPelo = colorPelo;
		}
		
		@Override
		public String toString() {
			
			return super.toString() + " ---- " + "Color de pelo: " + getColorPelo();
			
		}
		
}

package Animalillos;

public class Animal {
	
	int vida = 100;
		
	
	public int getVida() {
		return vida;
	}


	public void setVida(int vida) {
		this.vida = vida;
	}

	public String emiteSonido() {
		return "El " + super.toString() + " hace el sonido que le caracteriza.";
	}
	
	public String toString() {
		return "Animal-> Vida: " + this.getVida() + "\n";
	}
}
	

package inicio;

import Animalillos.*;
import coche.*;

public class Inicio {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		//inicio();
		inicioAnimal();			
		fin();
	}
	
	public static void inicioAnimal() {
		
		Animal animal = new Animal();
		System.out.println(animal);
		System.out.println(animal.emiteSonido());
		System.out.println("Clase para animal: " + animal.getClass());
		
		Animal can = new Perro();
		System.out.println(can);
		System.out.println(can.getClass().getSimpleName());
		System.out.println(can.emiteSonido());
		
		Perro can2 = new Perro();
		System.out.println(can2);
		System.out.println(can2.emiteSonido());
		
		Gato misino = new Gato();
		System.out.println(misino);
		System.out.println(misino.emiteSonido());
		
		System.out.println("Clase para animal: " + can2.getClass().getSimpleName());
		
		switch(can.getClass().getSimpleName()) {
		case "Perro":
			System.out.println("Un chusquel");
			break;
		case "Gato":
			System.out.println("Un misino");
			break;
		default:
			System.out.println("Un bicho");
			break;			
		}
		
		
		prueba(misino);
		prueba(can);
	}
	
	public static void prueba(Perro p)
	{
		System.out.println("La prueba del perro.");			
	}
	
	public static void prueba(Animal p)
	{
		System.out.println("La prueba del animal.");			
	}
	
	public static void prueba(Gato p)
	{
		System.out.println("La prueba de un Gatito.");			
	}
	
	public static void inicio()
	{
		
		Vehiculo v1 = new Automovil();
		v1.setMarca("Seat");
		((Automovil)v1).setConsumo(4.8); // Dar� error al compilar
		System.out.println(v1);
		
		Vehiculo v2 = new Todoterreno();
		v2.setMarca("Toyota");
		((Automovil)v2).setConsumo(7.0);
		
		Vehiculo v3 = new Todoterreno();
		v3.setMarca("Tesla");
		((Automovil)v3).setConsumo(12);
		
		Vehiculo v4 = new Bicicleta();
		((Bicicleta)v4).setTalla("Min�scula");				
		
		//System.out.println(v1.getConsumo());
		System.out.println(((Automovil)v2).getConsumo());
		System.out.println(v3.toString());
		System.out.println(v4.toString());
		
		Todoterreno v5 = new Todoterreno();
		v5.setMarca("Land Rover");
		v5.setConsumo(25);
		
		System.out.println("------ Ver Clases ----------");
		VerClase(v1);
		VerClase(v2);
		VerClase(v3);
		VerClase(v4);
		VerClase(v5);
		
	}
	
	public static void VerClase(Object objeto) {
		System.out.println(objeto.toString());
	}
	public static void fin()
	{
		
		System.out.println("\n\nPrograma finalizado.");
		
	}
}
